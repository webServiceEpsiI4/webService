
var cucumber = require('cucumber');
var rest = require("rest")
var mime = require("rest/interceptor/mime");
var bodyParser = require("body-parser");


var apiUrl = "http://172.17.0.2:3000/updateQuestion";

client = rest.wrap(mime);
var body;
var response;
var data;

cucumber.defineSupportCode(function({Given, Then,When}) {
   Given('The system expert have a question and find a response', function (tabResponse,callback) {

       data = tabResponse.hashes()[0];
       callback();


    });

    When('he send the answer to the server', function (callback) {
        // Write code here that turns the phrase above into concrete actions

        client({
            "path": apiUrl,
            "headers":{"Content-Type":"application/json;charset=UTF-8"},
            "method":"POST",
            "entity": data})
            .then(function(r) {
                response = r;
                callback();
            });

    });


    Then('the server said he recorded the answer', function (callback) {
        // Write code here that turns the phrase above into concrete actions
        if(response.status.code == 200) {
            callback();
        }
        else{
            throw new Error("Invalid HTTP status code " + response.status.code)
        }
    });


});
