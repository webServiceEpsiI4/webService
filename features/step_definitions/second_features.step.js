var cucumber = require('cucumber');
var rest = require("rest")
var mime = require("rest/interceptor/mime");
var bodyParser = require("body-parser");


var apiUrl = "http://172.17.0.2:3000/AllNextQuestion";

client = rest.wrap(mime);
var question1;


cucumber.defineSupportCode(function({Given, When, Then}) {
    var response;
    Given('A question exist', function (callback) {

        // Write code here that turns the phrase above into concrete actions

        var nextquestion;
        var test;
        client({
            "path": apiUrl,
            "headers": {"Content-Type": "application/json"},
            "method": "GET"
        })   .then(function (r) {
                response = r;
                callback();
            });

    });

    When('A question exist 1', function (callback) {
        if(response.status.code == 200) {
            question1 =response.entity['nextquestion'][1]['question']
            callback();
        }
        else{
            throw new Error("Invalid HTTP status code "+response )
        }
    });

    Then('The system expert get the next question', function (callback) {

        // Write code here that turns the phrase above into concrete actions

        apiUrl = "http://172.17.0.2:3000/nextQuestion";
        client({
            "path": apiUrl,
            "headers": {"Content-Type": "application/json"},
            "method": "POST"
        })   .then(function (r) {
            response = r;
            callback();
        });

    });

    Then('the next question become the awaiting question', function (callback) {

        // Write code here that turns the phrase above into concrete actions

        apiUrl = "http://172.17.0.2:3000/nextQuestion";
        client({
            "path": apiUrl,
            "headers": {"Content-Type": "application/json"},
            "method": "GET"
        })   .then(function (r) {
            response = r;
            callback();
        });

    });


    Then('the next question become the awaiting question 1', function (callback) {

        if(question1== response.entity['nextquestion']['question']){
            callback();
        }
        else{
            throw new Error("Invalid HTTP status code "+response.entity['nextquestion']['question']+ ' '+ question1)
        }

        //vérifier que next question est différente
    });
});
