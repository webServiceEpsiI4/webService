var cucumber = require('cucumber');
var rest = require("rest")
var mime = require("rest/interceptor/mime");

var apiUrl = "http://172.17.0.2:3000/question";

client = rest.wrap(mime);

cucumber.defineSupportCode(function({Given, When, Then}) {

    var newQuestion;
    var response;

    Given('a question with the following content:', function(question, callback) {
        newQuestion = question.hashes()[0];
        callback();
    });

    When('the user asks a question to the server', function (callback) {
        client({
            "path": apiUrl,
            "headers":{"Content-Type":"application/json"},
            "method":"POST",
            "entity": newQuestion})
            .then(function(r) {
                response = r;
                callback();
            });
    });

    Then('the question is created', function (callback) {
        if(response.status.code == 201) {
           callback();
        }
        else{
            throw new Error("Invalid HTTP status code " + response.status.code)
        }
    });


    });
