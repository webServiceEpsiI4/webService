/**
 * Created by Angus on 09/06/2017.
 */
var cucumber = require('cucumber');
var rest = require("rest")
var mime = require("rest/interceptor/mime");
var apiUrl = "http://172.17.0.2:3000/question";
var answerTest6;

client = rest.wrap(mime);

cucumber.defineSupportCode(function({Given, When, Then}) {

    var Question;
    var response;

    Given('The User set a question', function(Questiontable, callback) {
        Question = Questiontable.hashes()[0];
        client({
            "path": apiUrl,
            "headers":{"Content-Type":"application/json;charset=UTF-8"},
            "method":"POST",
            "entity": Question})
            .then(function(r) {
                response = r;
                callback();
            });

    });



    When('he change apiURI with Uri test 6', function (callback) {

        apiUrl ="http://172.17.0.2:3000/answer/"+ response.headers['Location'];
        client({
            "path": apiUrl,
            "headers": {"Content-Type": "application/json"},
            "method": "GET"
        })   .then(function (r) {
            response = r;
            callback();
        });

    });


    When('he find answer to our Question test 6', function (callback) {


        var str = response.entity;
        var re = /<div id="answer">.+<\/div>/i;
        var trouver = str.match(re);
        answerTest6= trouver[0].slice(17, trouver[0].length);
        answerTest6 = answerTest6.replace("</div>", "");


        if(answerTest6 != null) {
            callback();
        }
        else{
            throw new Error("Invalid HTTP status code "+nouvelleStr );
        }
    });



    Then('verifed is the anwser is ok test 6', function (callback) {


        if((answerTest6 != null) && (answerTest6 != "Réponse pas encore disponible !")) {
            callback();
        }
        else{
            throw new Error("Invalid HTTP status code " + response.status.code)
        }
    });


});
