/**
 * Created by Angus on 09/06/2017.
 */
/**
 * Created by Angus on 09/06/2017.
 */
var cucumber = require('cucumber');
var rest = require("rest")
var mime = require("rest/interceptor/mime");
var apiUrl = "http://172.17.0.2:3000/question";
var answerTest7;

client = rest.wrap(mime);

cucumber.defineSupportCode(function({Given, When, Then}) {

    var Question;
    var response;

    Given('The User set a question test7', function(Questiontable, callback) {
        Question = Questiontable.hashes()[0];
        client({
            "path": apiUrl,
            "headers":{"Content-Type":"application/json;charset=UTF-8"},
            "method":"POST",
            "entity": Question})
            .then(function(r) {
                response = r;
                callback();
            });
    });


    When('he change apiURI with Uri test7', function (callback) {

        apiUrl ="http://172.17.0.2:3000/answer/"+ response.headers['Location'];
        client({
            "path": apiUrl,
            "headers": {"Content-Type": "application/json"},
            "method": "GET"
        })   .then(function (r) {
            response = r;
            callback();
        });

    });


    When('the server expert can t find a response test7', function (callback) {


        var str = response.entity;
        var re = /<div id="answer">.+<\/div>/i;
        var trouver = str.match(re);
        answerTest7= trouver[0].slice(17, trouver[0].length);
        answerTest7 = answerTest7.replace("</div>", "");


        if(answerTest7 != null) {
            callback();
        }
        else{
            throw new Error("Invalid HTTP status code "+answerTest7 );
        }
    });



    Then('he notified is fail test7', function (callback) {


        if(answerTest7 == "pas de réponse connue") {
            callback();
        }
        else{
            throw new Error("Invalid HTTP status code " + answerTest7)
        }
    });


});
