var cucumber = require('cucumber');
var rest = require("rest")
var mime = require("rest/interceptor/mime");
var bodyParser = require("body-parser");


var apiUrl = "http://172.17.0.2:3000/nextquestion";

client = rest.wrap(mime);


cucumber.defineSupportCode(function({Given, When, Then}) {
    var response;
    Given('The system expert ask for the next question', function (callback) {

        // Write code here that turns the phrase above into concrete actions

        var nextquestion;
        var test;
        client({
            "path": apiUrl,
            "headers": {"Content-Type": "application/json"},
            "method": "GET"
        })   .then(function (r) {
            response = r;
            callback();
        });

    });

    When('the server said there is no question', function (callback) {

        if(response.entity['nextquestion']['question'] == "no question") {
            callback();
        }
        else{
            throw new Error("Il existe déjà une question "+response.entity['nextquestion']);
        }
    });


  Then('the system expert set him in sleep mod', function (callback) {
      // Write code here that turns the phrase above into concrete actions



      callback(null, 'sleep Mode');


    });


});
