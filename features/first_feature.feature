Feature: Registration of questions

  Scenario: To save a question by the server
    Given a question with the following content:
      |name       |question|
      |very       |good    |
    When the user asks a question to the server
    Then the question is created