var express = require('express');
var mysql   = require("mysql");
var md5 = require('MD5');
var router = express.Router();
var app=express();
var bodyParser = require("body-parser");
var questionList = [];
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//---------------------------------------Route pour l'API Web------------------------------------------------------
router.get('/', function(req, res) {
  res.status(200).render('index', { title: 'Web-Service' });
});

router.post('/question', function(req,res){

    var client=mysql.createConnection({
        host     : '172.17.0.2',
        user     : 'root',
        password : 'rootroot',
        database : 'questionreponse'
    });
    var URIFind = md5(req.body.name+'/'+req.body.question+'/');
    var insert= [req.body.name,req.body.question, URIFind];

    client.query("INSERT INTO question (name, question, uri) VALUES (?,?,?);",insert,function(error, results) {

        if (error) {
            res.status(500);
        } else {
            var Id = results.insertId;
            client.query('SELECT answer from question WHERE answer !="NULL" AND question LIKE "%'+req.body.question+'%" GROUP BY answer ORDER BY answer DESC',function(error, results) {
                if (results && results[0] && results[0].answer && results[0].answer!=null){

                    res.status(201).render('yourResult', {answer: results[0].answer, question: req.body.question});

                }else{

                    var data = {Id :Id,name:req.body.name,question:req.body.question,uri: URIFind};
                    questionList.push(data);
                    res.setHeader('Location',URIFind);
                    res.status(201).render('findResult',{data: data});

                }
            });
        }
    });
    client.end();
});

router.get('/answer/:URI', function(req, res) {

    var client=mysql.createConnection({
        host     : '172.17.0.2',
        user     : 'root',
        password : 'rootroot',
        database : 'questionreponse'
    });
    client.query('SELECT * from question WHERE answer !="NULL" AND uri= "'+req.params.URI+'" GROUP BY answer ORDER BY answer DESC' , function (error, results) {

        if (results && results[0] && results[0].answer && results[0].answer!=''){
            res.status(200).render('answer', {answer: results[0].answer});
        }else{

            res.status(200).render('answer', {answer:"Reponse pas encore disponible !"});
        }

    });
    client.end();
});

//---------------------------------------Route pour le SE------------------------------------------------------

router.post('/nextquestion', function(req, res) {
    if (questionList.length == 0){
        res.json({nextquestion : "no question"});

    }else{
        res.json({nextquestion : questionList.shift()});
    }
});

router.get('/nextquestion', function(req, res) {
    if (questionList.length == 0){
        res.json({nextquestion :{"Id":10000000,"name":"42","question":"no question","uri":"none"}});

    }else{
        res.json({nextquestion : questionList[0]});
    }
});

router.get('/AllNextQuestion', function(req, res) {
    res.json({nextquestion : questionList});
});

router.post('/updateQuestion', function(req, res) {
    var client = mysql.createConnection({
        host: '172.17.0.2',
        user: 'root',
        password: 'rootroot',
        database: 'questionreponse'

    });

    var update = [req.body.answer,req.body.id];

    client.query("UPDATE question SET answer=? WHERE id = ?", update, function (error, results) {

        if (error) {
            res.status(500)
        } else {
            res.status(200).render('findResult',{data: update});
        }

    });
    client.end();
});
//--------------------------------------------------------------------------------------------------------------
module.exports = router;
